var myApp = angular.module('infoLeyend',['smoothScroll','ngDialog']);

	myApp.controller('infoLeyendController', function($scope, smoothScroll,$timeout,ngDialog) {

		//default
		$scope.elBorrar 	= 	null;
		$scope.textoLuego 	= 	false;
		$scope.scaleGlobal 	= 	1;
		$scope.xGlobal 		= 	0;
		$scope.yGlobal 		= 	0;

		//se selecciona la imagen del lienzo y guarda en variable
		var img = document.getElementById('lienzoImagen');

		$scope.goWebSite = function(web){
			window.open(web,'_blank');
		}

		$scope.calcularAlto = function(top) {

			//var altoVentana = document.body.clientHeight;
			//var altoVentana = window.innerHeight - 100;
			//var altoVentana = img.clientHeight;
			var altoVentanaAbajo = img.clientHeight - document.body.scrollTop*2;
			var altoVentanaArriba = img.clientHeight + document.body.scrollTop;

			var offset = 140;

			//si esta arriba o abajo...
			if(top > 50){

				var result = altoVentanaAbajo * Math.floor(top) / 100 - offset;

				return result;

			}else{

				var result = ((altoVentanaArriba * Math.floor(top) / 100) - altoVentanaArriba + 340 ) * -1;

				return result;

			};

		}


		$scope.mitadPantalla = 500;

		$scope.dialogar = function(tipo,obj){

			if(obj){

				$scope.elBorrar = obj;

			};

			ngDialog.open({
				template: tipo,
				scope: $scope
			})
			
		}

		$scope.escalarPuntos = function(puntos){

			for (var i = puntos.length - 1; i >= 0; i--) {

				puntos[i].scalePoint = $scope.nuevaScale;

			};

			$scope.upLocalStorage();

		};

		$scope.actualizarPuntos = function(puntos){


			for (var i = puntos.length - 1; i >= 0; i--) {
				
				//si donde he pinchado es menor que la mitad.. IZQUIERDA
				if(puntos[i].left < 50){

					//izquierda
					puntos[i].lado = 'borIzquierda';

				}else{
					
					//derecha
					puntos[i].lado = 'borDerecho';

				};

				if(puntos[i].top > 50){

					//abajo
					puntos[i].lado = puntos[i].lado + ' borDeabajo';

				};

				//console.log(puntos[i].nombre+' es '+puntos[i].lado);

				$scope.upLocalStorage();

			};


		};

		$scope.encuadrarPunto = function (pos) {

			if(pos != 'last'){

				var element = document.getElementById('punto-'+pos);

			}else{

				var posi = $scope.muralesActivos[$scope.idMuralActivo].puntos.length-1;

				//console.log(posi);

				var element = document.getElementById('punto-'+posi);

			}

			element.className = 'listaPuntosEdit select';

			$timeout(function() {
		    
		    	element.className = 'listaPuntosEdit';
		    
		    }, 800);

			var options = {
			    duration: 500,
			    easing: 'easeInOutQuint',
			    offset: 150,
			    containerId: 'floating-puntos'
			}

			smoothScroll(element, options);

		}
		

		$scope.newScale = function (newSc) {
			
			$scope.scaleGlobal = newSc;

			if($scope.scaleGlobal == 1){

				$scope.xGlobal = 0;
				$scope.yGlobal = 0;
				
			};

		}

		$scope.resetLienzo = function () {
			
			$scope.yGlobal = 0;
			$scope.xGlobal = 0;
			$scope.scaleGlobal = 1;

		}

		$scope.ordenacion = 'nombre';

		$scope.titleApp = 'Editor';

		//objeto que recoge los puntos
		$scope.puntos = [

			{nombre:'', txt: '', top:-37, left:-26}

		];

		$scope.idMuralActivo = 0;

		var muralEjemplo1 = {

			id: 0,
			nombre : 'Mural de ejemplo 1',
			foto: 'http://2.bp.blogspot.com/_EZ16vWYvHHg/S-Bl2fuyyWI/AAAAAAAAMKc/DNayYJK8mEo/s1600/www.BancodeImagenesGratuitas.com-Fantasticas-20.jpg',
			puntos : [

			{nombre:'cielo', txt: 'mira', top:31, left:46, scalePoint:0.8},
			{nombre:'globo', txt: 'colorido', top:4, left:18, scalePoint:0.8},
			{nombre:'montaña', txt: 'verde', top:38, left:24, scalePoint:0.8}

			]

		};


		var muralEjemplo2 = {

			id: 1,
			nombre : 'Mural de ejemplo 2',
			foto: 'http://www.imagenes-juegos.com/halo-4-imagen-i306424-i.jpg',
			puntos : [

			{nombre:'centro', txt: 'en el medio', top:24, left:40, scalePoint:0.8},
			{nombre:'icono', txt: 'icono azul', top:70, left:6, scalePoint:0.8},
			{nombre:'mirilla', txt: 'pa mirar', top:60, left:48, lado:'borDerecho', scalePoint:0.8},
			{nombre:'balas', txt: 'cuantas quedan', top:76, left:72, lado:'borDerecho', scalePoint:0.8},
			{nombre:'herramienta', txt: 'pa construir', top:15, lado:'borDerecho', left:79, scalePoint:0.8}

			]

		};

		//declaramos el array de los murales vacio
		$scope.muralesActivos = [];

		//le añadimos el mural de ejemplo
		$scope.muralesActivos.push(muralEjemplo1);
		$scope.muralesActivos.push(muralEjemplo2);



		$scope.cargarMural = function (pos){

			$scope.sectionActiveInfo = false;

			$scope.resetLienzo();

			$scope.idMuralActivo = pos;

			$scope.sectionActiveMurales = false;

			$scope.titleApp = $scope.muralesActivos[$scope.idMuralActivo].nombre;

			//actualizamos los puntos
			$scope.actualizarPuntos($scope.muralesActivos[$scope.idMuralActivo].puntos);

			$scope.upLocalStorage();

		}

		$scope.copiarMural = function(mural,pos){

			//fruits.splice(2, 1, "Lemon", "Kiwi")

			$scope.muralesActivos.splice(pos, 0, mural);

		}

		$scope.nuevoMural = function () {
			
			$scope.titleApp = 'Nuevo mural creado';

			var nuevoMuralObj = {
				id: null,
				nombre : $scope.tituloNewMural,
				foto: $scope.fotoNewMural,
				puntos:[]
			};

			$scope.muralesActivos.push(nuevoMuralObj);

			$scope.cargarMural($scope.muralesActivos.length - 1);

		}

		/*-------------------------------------------------------------------------------------*/

		//$scope.puntos.splice(0, 1);
		$scope.puntos.pop();


		//si existen la imagen guardada...
		if(localStorage.getItem('idMuralActivo')){

			$scope.idMuralActivo = parseInt(localStorage.getItem('idMuralActivo'));

		};

		//si existen la imagen guardada...
		if(localStorage.getItem('muralesActivos')){

			$scope.muralesActivos = JSON.parse(localStorage.getItem('muralesActivos'));

		};


		$scope.activar = function (){

			document.onkeydown = tecla;

		};

		$scope.desactivar = function (){

			document.onkeydown = null;

		};

		$scope.activar();

		function tecla(e){

			e.preventDefault();
			
			var num = e.keyCode;

			// alert(num);

			//uno
			if(num === 49){

				$scope.$apply(function () {

					$scope.sectionActivePuntos = !$scope.sectionActivePuntos;
					$scope.sectionActiveMural = false;
					$scope.sectionActiveExport = false;
					$scope.sectionActiveMurales = false;

				});

			};

			//dos
			if(num === 50){

				$scope.$apply(function () {

					$scope.sectionActiveMural = !$scope.sectionActiveMural;
					$scope.sectionActivePuntos = false;
					$scope.sectionActiveExport = false;
					$scope.sectionActiveMurales = false;

				});

			};

			//tres
			if(num === 51){

				$scope.$apply(function () {

					$scope.sectionActiveExport = !$scope.sectionActiveExport;
					$scope.sectionActivePuntos = false;
					$scope.sectionActiveMural = false;
					$scope.sectionActiveMurales = false;

				});

			};

			//cuadro
			if(num === 52){

				$scope.$apply(function () {

					$scope.sectionActiveMurales = !$scope.sectionActiveMurales;
					$scope.sectionActivePuntos = false;
					$scope.sectionActiveMural = false;
					$scope.sectionActiveExport = false;

				});

			};

			//delete
			if(num === 46){

				$scope.$apply(function () {

					$scope.dialogar('borrarMural',$scope.muralesActivos[$scope.idMuralActivo]);

				});

			};


			//next
			if(num === 33){

				$scope.$apply(function () {

					if($scope.idMuralActivo != 0){

						$scope.cargarMural($scope.idMuralActivo - 1);

					};

				});

			};

			//prev
			if(num === 34){

				$scope.$apply(function () {

					if($scope.muralesActivos.length != $scope.idMuralActivo+1){

						$scope.cargarMural($scope.idMuralActivo + 1);

					};

				});

			};

			//derecha
			if(num === 39){

				$scope.$apply(function () {

					if($scope.scaleGlobal>1){

						$scope.xGlobal = $scope.xGlobal - (200 / $scope.scaleGlobal);

					};

				});

			};

			//izquierda
			if(num === 37){

				$scope.$apply(function () {

					if($scope.scaleGlobal>1){

						$scope.xGlobal = $scope.xGlobal + (200 / $scope.scaleGlobal);

					};

				});

			};

			//ariba
			if(num === 38){

				$scope.$apply(function () {

					if($scope.scaleGlobal>1){

						$scope.yGlobal = $scope.yGlobal + (200 / $scope.scaleGlobal);

					};

				});

			};

			//abajo
			if(num === 40){

				$scope.$apply(function () {

					if($scope.scaleGlobal>1){

						$scope.yGlobal = $scope.yGlobal - (200 / $scope.scaleGlobal);

					};

				});

			};

			//mas
			if(num === 107 || num === 187){

				$scope.$apply(function () {

					$scope.newScale($scope.scaleGlobal + 1);


				});

			};

			//reset
			if(num === 48){

				$scope.$apply(function () {

					$scope.resetLienzo();

				});

			};

			//menos
			if(num === 109 || num === 189){

				$scope.$apply(function () {

					if($scope.scaleGlobal>1){
						
						$scope.newScale($scope.scaleGlobal - 1);
					
					};

				});

			};

		}

		$scope.tituloInput = 'Nada..';
		$scope.textoInput = 'Nada..';


		$scope.mostrarDialogo = function(e){
			
			//Para saber si esta a la izquierda o a la derecha

			//obtengo la mitad de la imagen
			var mitadVertical = img.clientWidth/2;
			var mitadHorizontal = img.clientHeight/2;

			//si donde he pinchado es menor que la mitad.. IZQUIERDA
			if( e.offsetX < mitadVertical ){

				var seccionLago = 'borIzquierda';

			//sino... DERECHA
			}else{
				
				seccionLago = 'borDerecho';

			};

			if( e.offsetY > mitadHorizontal ){

				seccionLago = seccionLago + ' borDeabajo';

			};

			//se extraen las coordenadas del click
			var leftNewPoint = 100 * e.offsetX / img.clientWidth;
			var topNewPoint = 100 * e.offsetY / img.clientHeight;

			//para que se empequeñezca a medida que se crean con mas zoom
			var escalaPunto = 0.8 / $scope.scaleGlobal;

			//punto generado
			
			if($scope.textoLuego){

				var puntoVacio = {nombre:'Punto vacio',txt:'', top:topNewPoint, left:leftNewPoint, lado:seccionLago, scalePoint:escalaPunto};

				//generamos automaticamente el punto con datos dummy
				$scope.crearNuevoPunto(puntoVacio);

			}else{

				$scope.nuevoPunto = {nombre:$scope.tituloInput,txt:$scope.textoInput, top:topNewPoint, left:leftNewPoint, lado:seccionLago, scalePoint:escalaPunto};

				//mostramos el dialogo

				$scope.tituloInput = '';
				$scope.textoInput = '';

				$scope.mostrarDialog = true;

			}
				
		};

		$scope.resaltarEnLienzo = function(pos){

			var element = document.getElementById('p-'+pos);

			element.className = 'marcoSelector remarcado';

		}


		$scope.noResaltarEnLienzo = function(pos){

			var element = document.getElementById('p-'+pos);

			element.className = 'marcoSelector';

		}


		$scope.crearNuevoPunto = function(punto) {

			//actualizamos con lo que se ha escrito en el dialogo
			if(!$scope.textoLuego){

				punto.nombre = $scope.tituloInput;
				punto.txt = $scope.textoInput;

			};

			//se añade el nuevo punto al array
		    $scope.muralesActivos[$scope.idMuralActivo].puntos.push(punto);

		    //se actualiza
			$scope.upLocalStorage();

			//ocultamos el dialogo
			$scope.mostrarDialog = false;	    

			//bajamos pa verlo DA ERROR
		    //$scope.encuadrarPunto('last');

		};


		$scope.datosExtrac = {};

		$scope.importarMurales = function(tipo){

			if(tipo == 'add'){

				$scope.muralesActivos = $scope.muralesActivos.concat(JSON.parse($scope.muralesImportados));


			}else{

				$scope.muralesActivos = JSON.parse($scope.muralesImportados);

				$scope.idMuralActivo = 0;

			};
			

			$scope.upLocalStorage();

			$scope.muralesImportados = '';

		}

		$scope.importarMural = function(){

			if(Array.isArray(JSON.parse($scope.muralImportado))){

				//si es un array de murales..

				$scope.muralesImportados = $scope.muralImportado;

				$scope.dialogar('importarMurales');

			}else{

				//no es un array

				$scope.muralesActivos.push(JSON.parse($scope.muralImportado));

				$scope.upLocalStorage();

			};

			$scope.muralImportado = '';

		}

		function calcularTop(topInput){

			var topOutput = topInput * 8;

			return topOutput;

		}

		$scope.abrirVentana = function(url){
			window.open(url,'_blank');
			
		}

		$scope.encuadrar = function (top, left) {

			$scope.scaleGlobal = 5;
			$scope.xGlobal = left;
			$scope.yGlobal = calcularTop(top);

		}

		$scope.resetearTodo = function(){

			localStorage.clear();

			location.reload(true);

		}

		$scope.upLocalStorage = function(){

			//actualizamos los puntos
			localStorage.setItem('muralesActivos', JSON.stringify($scope.muralesActivos));

			localStorage.setItem('idMuralActivo', $scope.idMuralActivo);

		}

		$scope.removeMural = function (mural) {

			var index = $scope.muralesActivos.indexOf(mural);

			$scope.muralesActivos.splice(index,1);

			$scope.idMuralActivo = 0;

			$scope.upLocalStorage();

			$scope.elBorrar = null;

		}

		//para borrar puntos
		$scope.remove = function(item) {

			//si recibe un item, borra un solo
			if(item){

				var index = $scope.muralesActivos[$scope.idMuralActivo].puntos.indexOf(item);

				$scope.muralesActivos[$scope.idMuralActivo].puntos.splice(index, 1);

			//si no.. borralos todos.
			}else{

				//vacio los puntos del mural activo
				$scope.muralesActivos[$scope.idMuralActivo].puntos = [];

			};

			$scope.upLocalStorage();

		};

	});


	myApp.filter('cuantosMurales', function() {

	    return function(txt) {

	       	txt = JSON.parse(txt);

	        return txt.length;
	    };

	});


	myApp.directive('imageonload', function() {

    return {

        link: function(scope, element, attrs) {

			this.className = "nada";

	         element.bind('load', function() {

				this.className = "imgLienzo";

	          });
        
        	}
    	};
	});